<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Controller\Learning;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Index class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class Index extends Action
{
    /**
     * @var \Magecom\Learning\Model\Learning
     */
    protected $_resultPageFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * Index constructor.
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        PageFactory $resultPageFactory
    ) {
        $this->_objectManager = $objectManager;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $page = $this->_resultPageFactory->create();

        return $page;
    }
}
