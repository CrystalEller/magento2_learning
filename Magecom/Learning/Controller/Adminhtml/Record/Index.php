<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Controller\Adminhtml\Record;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;

/**
 * Index class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class Index extends Action
{
    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $_resultPage;
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Index constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $this->_resultPage = $this->_resultPageFactory->create();

        $this->_setTitleMenuBreadcrumbs();

        return $this->_resultPage;
    }

    /**
     * Sets page title, sets active menu, sets breadcrumbs
     */
    protected function _setTitleMenuBreadcrumbs()
    {
        /**
         * Set active menu item
         */
        $this->_resultPage->setActiveMenu('Magecom_Learning::magecom');
        $this->_resultPage->getConfig()->getTitle()->prepend(__('Manage learning records'));

        /**
         * Add breadcrumb item
         */
        $this->_resultPage->addBreadcrumb(__('Magecom_Singularis'), __('Manage records'));
    }

    /**
     * Is the user allowed to view the grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magecom_Learning::magecom');
    }
}
