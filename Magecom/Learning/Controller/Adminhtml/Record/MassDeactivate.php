<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Controller\Adminhtml\Record;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magecom\Learning\Model\System\Config\State;

/**
 * MassDeactivate class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class MassDeactivate extends Action
{
    /**
     * MassDeactivate constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * MassDeactivate action
     */
    public function execute()
    {
        $recordsIds = $this->getRequest()->getParam('selected');

        if (!is_array($recordsIds)) {
            $this->messageManager->addError(__('Please select records.'));
        } else {
            try {
                foreach ($recordsIds as $recordId) {
                    $model = $this->_objectManager->create('Magecom\Learning\Model\Learning')->load($recordId);
                    $model->setStatus(State::NOT_ACTIVE);
                    $model->save();
                }
                $this->messageManager->addSuccess(__('Total of %1 record(s) were deactivated.', count($recordsIds)));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('magecom_learning/record/index');
    }

    /**
     * Does the user have access.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magecom_Learning::magecom');
    }


}