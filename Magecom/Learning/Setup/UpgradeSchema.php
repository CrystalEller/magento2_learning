<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * UpgradeSchema class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const TABLE_NAME_LEARNING = 'magecom_learning';

    /**
     * Main method that is called when upgrade is running
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if(!$context->getVersion()) {
            $this->_upgrade002($installer);
        }

        // Should be run only for 0.0.1 - 0.0.2 upgrade
        if (version_compare($context->getVersion(), '0.0.2', '=')) {
            $this->_upgrade002($installer);
        }

        $installer->endSetup();
    }

    /**
     * @param SchemaSetupInterface $installer
     */
    private function _upgrade002($installer)
    {
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($installer->getTable(self::TABLE_NAME_LEARNING))) {
            // Declare data
            $columns = [
                'status' => [
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable'  => false,
                    'comment'   => 'status',
                ],
            ];

            $connection = $installer->getConnection();

            foreach ($columns as $name => $definition) {
                $connection->addColumn($installer->getTable(self::TABLE_NAME_LEARNING), $name, $definition);
            }

            $connection->query('TRUNCATE TABLE ' . self::TABLE_NAME_LEARNING);
        }
    }
}