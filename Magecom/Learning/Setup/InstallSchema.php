<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * InstallSchema class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class InstallSchema implements InstallSchemaInterface
{
    const TABLE_NAME_LEARNING = 'magecom_learning';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        // Check if table exist first
        if (!$installer->getConnection()->isTableExists($installer->getTable(self::TABLE_NAME_LEARNING))) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable(self::TABLE_NAME_LEARNING))
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn('title', Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Title')
                ->addColumn('content', Table::TYPE_TEXT, '1M', ['nullable' => true, 'default' => null], 'Content')
                ->addColumn('url_key', Table::TYPE_TEXT, 255, [], 'Url Key')
                ->addColumn('creation_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Creation Time')
                ->addColumn('update_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Update Time')
                ->setComment('Magecom Learning');

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
