<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * UpgradeData class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class UpgradeData implements UpgradeDataInterface
{
    const TABLE_NAME_LEARNING = 'magecom_learning';

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if(!$context->getVersion()) {
            $this->_upgradeData002($setup);
        }

        // Should be run only for 0.0.2 - 0.0.3 upgrade
        if (version_compare($context->getVersion(), '0.0.2', '=')) {
            $this->_upgradeData002($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    private function _upgradeData002($setup)
    {
        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($setup->getTable(self::TABLE_NAME_LEARNING))) {
            $data = array(
                array(
                    'title' => 'TITLE',
                    'content' => 'THIS IS CONTENT',
                    'url_key' => 'THIS IS URL KEY',
                ),
                array(
                    'title' => 'TITL',
                    'content' => 'THIS IS CONTEN',
                    'url_key' => 'THIS IS URL KE',
                ),
                array(
                    'title' => 'ccccc',
                    'content' => 'vvvvvvvv',
                    'url_key' => 'bbbbbbbbbbbb',
                ),
            );
            $setup->getConnection()->insertMultiple($setup->getTable(self::TABLE_NAME_LEARNING), $data);
        }
    }
}