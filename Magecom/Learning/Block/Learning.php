<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Block;

/**
 * Learning class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class Learning extends \Magento\Framework\View\Element\Template
{
    protected $_helper;

    protected $_objectManager;

    /**
     * First constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        parent::__construct($context, $data);

        $this->_objectManager = $objectManager;
    }

    /**
     * @return \Magecom\Learning\Model\Resource\Learning\Collection
     */
    public function getLearningCollection()
    {
        $model = $this->_objectManager->create('Magecom\Learning\Model\Learning');
        $collection = $model->getCollection();

        return $collection;
    }
}
