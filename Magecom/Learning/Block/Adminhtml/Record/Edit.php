<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Block\Adminhtml\Record;

use Magento\Backend\Block\Widget\Form\Container;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

/**
 * Edit class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class Edit extends Container
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(Context $context, Registry $registry, array $data = [])
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize record edit block
     */
    protected function _construct()
    {
        $this->_objectId = 'record_id';
        $this->_blockGroup = 'Magecom_Learning';
        $this->_controller = 'adminhtml_record';

        $record = $this->_coreRegistry->registry('record');

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save record'));
        //$this->buttonList->update('delete', 'label', __('Delete record'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and continue edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                    ],
                ]
            ],
            -100
        );

        if ($record->getId()) {
            $this->addButton(
                'delete', ['label' => __('Delete'), 'onclick' => 'deleteConfirm(' . json_encode(
                        __('Are you sure you want to do this?')
                    ) . ',' . json_encode(
                        $this->getUrl(
                            'magecom_learning/*/delete', ['record_id' => $record->getId()]
                        )
                    ) . ')', 'class' => 'scalable delete',], 10, 1
            );
        }
    }

    /**
     * Retrieve text for header element depending on loaded post
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('record')->getId()) {
            return __("Edit Post '%1'", $this->escapeHtml($this->_coreRegistry->registry('record')->getTitle()));
        } else {
            return __('New Post');
        }
    }

    /**
     * Getter of url for "Save and Continue" button
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('magecom_learning/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }

    /**
     * @param array $args
     * @return string
     */
    public function getDeleteUrl(array $args = [])
    {
        $params = array_merge($this->getDefaultUrlParams(), $args);
        return $this->getUrl('magecom_learning/*/delete', $params);
    }
}
