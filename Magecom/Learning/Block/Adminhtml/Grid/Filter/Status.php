<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Block\Adminhtml\Grid\Filter;

use Magecom\Learning\Model\System\Config\State;

/**
 * Status class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class Status extends \Magento\Backend\Block\Widget\Grid\Column\Filter\Select
{
    /**
     * Get grid options
     *
     * @return array
     */
    protected function _getOptions()
    {
        return [
            ['label' => '', 'value' => ''],
            ['label' => __('Active'), 'value' => State::ACTIVE],
            ['label' => __('Disable'), 'value' => State::NOT_ACTIVE],
            ['label' => __('Archive'), 'value' => State::ARCHIVE]
        ];
    }

    /**
     * Get condition
     *
     * @return int
     */
    public function getCondition()
    {
        if ($this->getValue() == State::ACTIVE) {
            return State::ACTIVE;
        } elseif ($this->getValue() == State::NOT_ACTIVE) {
            return State::NOT_ACTIVE;
        } else {
            return State::ARCHIVE;
        }
    }
}
