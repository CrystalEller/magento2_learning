<?php
/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package Magecom_Module
 * @copyright Copyright (c) 2016 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Magecom\Learning\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * State class
 *
 * @category Magecom
 * @package Magecom_Learning
 * @author  Magecom
 */
class State implements ArrayInterface
{
    const NOT_ACTIVE  = 0;

    const ACTIVE = 1;

    const ARCHIVE = 2;

    /**
     * @var array
     */
    protected $_options;

    /**
     * Initialize the options array
     */
    public function __construct()
    {
        $this->_options = [
            ['value' => self::NOT_ACTIVE, 'label' => __('Not active')],
            ['value' => self::ACTIVE, 'label' => __('Active')],
            ['value' => self::ARCHIVE, 'label' => __('Archive')],
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_options;
    }
}